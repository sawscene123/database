package com.example.saw.database;

/**
 * Created by saw on 12/19/2017.
 */

public class StudentDto {
    private int id;
    private String name;
    private int rollNo;
    private float marks;

    public StudentDto(){

    }

    public StudentDto(int id, String name, int rollNo, float marks){
        this.id = id;
        this.name = name;
        this.rollNo = rollNo;
        this.marks = marks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public float getMarks() {
        return marks;
    }

    public void setMarks(float marks) {
        this.marks = marks;
    }
}
