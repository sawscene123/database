package com.example.saw.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saw on 12/19/2017.
 */

public class StudentDao {
    private DbOpenHelper dbOpenHelper;
    private SQLiteDatabase database;
    private String[] allColums = new String[]{
            DbOpenHelper.COLUMN_ID,
            DbOpenHelper.COLUMN_NAME,
            DbOpenHelper.COLUMN_ROLL_NO,
            DbOpenHelper.COLUMN_MARKS
    };

    public StudentDao(Context context) {
        dbOpenHelper = new DbOpenHelper(context);
    }

    //CRUD -> Create, Read, Update, Delete

    //Create operation
    public void insert(StudentDto studentDto) {

        database = dbOpenHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DbOpenHelper.COLUMN_NAME, studentDto.getName());
        values.put(DbOpenHelper.COLUMN_ROLL_NO, studentDto.getRollNo());
        values.put(DbOpenHelper.COLUMN_MARKS, studentDto.getMarks());
        try {
            database.insert(DbOpenHelper.TABLE_STUDENT, null, values); //insert into tbl_student (_name, _roll_no, _marks) values (student.getName,....)
        } finally {
            database.close();
            dbOpenHelper.close();
        }
    }

    //Read Operation -> Select query
    public List<StudentDto> getAllStudent() {
        database = dbOpenHelper.getReadableDatabase();
        List<StudentDto> studentDtoList = new ArrayList<>();

        Cursor cursor = database.query(DbOpenHelper.TABLE_STUDENT, allColums,
                null, null, null, null, null);//select * from tbl_student
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    float marks = cursor.getFloat(cursor.getColumnIndex(DbOpenHelper.COLUMN_MARKS));
                    int id = cursor.getInt(cursor.getColumnIndex(DbOpenHelper.COLUMN_ID));//cursor.getInt(0);
                    int rollNo = cursor.getInt(2);
                    String name = cursor.getString(1);
                    StudentDto studentDto = new StudentDto(id, name, rollNo, marks);
                    studentDtoList.add(studentDto);
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }

        database.close();
        dbOpenHelper.close();
        return studentDtoList;
    }

    public StudentDto getStudentById(int id){
        database = dbOpenHelper.getReadableDatabase();
        StudentDto studentDto = null;

        Cursor cursor = database.query(DbOpenHelper.TABLE_STUDENT, allColums, DbOpenHelper.COLUMN_ID +"=? or " + DbOpenHelper.COLUMN_NAME +"=?",
                new String[]{String.valueOf(id), name}, null, null, null);
        Cursor cursor1 = database.rawQuery("select * from " + DbOpenHelper.TABLE_STUDENT +" where " + DbOpenHelper.COLUMN_ID +"=" + id, null);

    }

    public void update(){


        database = dbOpenHelper.getWritableDatabase();

        ContentValues values = new ContentValues();

        database.update(DbOpenHelper.TABLE_STUDENT, values, DbOpenHelper.COLUMN_ID +"=?", new String[]{"1"});
    }
}
